/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package misclases;
import java.util.Scanner;
/**
 *
 * @author hp
 */
public class TextCotizacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        Cotizacion cotizacion =new Cotizacion();
        Scanner sc = new Scanner(System.in);
        int opcion=0, numCotizacion = 0, plazo = 0;
        String descripcionAutomovil = " ";
        float precio = 0.0f, porcentajePagoInicial = 0.0f;
        do{
            System.out.println("1. Iniciar objeto");
            System.out.println("2. cambiar numero de cotizacion");
            System.out.println("3. cambiar descripcion");
            System.out.println("4. cambiar precio");
            System.out.println("5. cambiar porcentaje del pago inicial");
            System.out.println("6. cambiar plazo");
            System.out.println("7. imprimir");
            System.out.println("8. salir");
            System.out.println(" dame una opcion");
            
            opcion = sc.nextInt();
            switch(opcion){
                case 1:
                    System.out.println("Dame el numero de cotizacion: ");
                    numCotizacion = sc.nextInt();
                    System.out.println("Dame la descripcion del automovil: ");
                    descripcionAutomovil = sc.next();
                    System.out.println("Dame el precio: ");
                    precio = sc.nextFloat();
                    System.out.println("Dame el porcentaje del pago inicial");
                    porcentajePagoInicial = sc.nextFloat();
                    System.out.println("Dame el plazo");
                    plazo = sc.nextInt();
                    
                    cotizacion.setNumCotizacion(numCotizacion);
                    cotizacion.setDescripcionAutomovil(descripcionAutomovil);
                    cotizacion.setPrecio(precio);
                    cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                    cotizacion.setPlazo(plazo);
                    break;
                case 2:
                    System.out.println("Dame el numero de cotizacion: ");
                    numCotizacion = sc.nextInt();
                    cotizacion.setNumCotizacion(numCotizacion);
                    break;
                    
                case 3:
                    System.out.println("Dame la descripcion del automovil: ");
                    descripcionAutomovil = sc.next();
                    cotizacion.setDescripcionAutomovil(descripcionAutomovil);
                    break;    
                    
                case 4:
                    System.out.println("Dame el precio: ");
                    precio = sc.nextFloat();
                    cotizacion.setPrecio(precio);
                    break;
                    
                case 5:
                    System.out.println("Dame el porcentaje del pago inicial");
                    porcentajePagoInicial = sc.nextFloat();
                    cotizacion.setPorcentajePagoInicial(porcentajePagoInicial);
                    break;
                    
                case 6:
                    System.out.println("Dame el plazo");
                    plazo = sc.nextInt();
                    cotizacion.setPlazo(plazo);
                    break;
                    
                case 7:
                    cotizacion.imprimirCotizacion();
                    break;    
                    
                case 8:
                    System.out.println("Hasta la vista baby ........!!");
                    break;  
    
                default:
                    System.out.println("No es una opcion valida, usa del 1-8 ");    
            }   
        }while(opcion!=8);
        
    }
    
}
